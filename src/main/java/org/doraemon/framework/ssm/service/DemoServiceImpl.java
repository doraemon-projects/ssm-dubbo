package org.doraemon.framework.ssm.service;

import org.springframework.stereotype.Service;

/**
 * @description: 描述
 * @author: fengwenping
 * @date: 2021-11-06 17:22
 */
@Service("demoServiceImpl")
public class DemoServiceImpl implements DemoService {
    @Override
    public String sayHello(String name) {
        return "hello, my name is " + name;
    }
}
