package org.doraemon.framework.ssm.service;

/**
 * @description: 描述
 * @author: fengwenping
 * @date: 2021-11-06 17:06
 */
public interface DemoService {

    String sayHello(String name);
}
