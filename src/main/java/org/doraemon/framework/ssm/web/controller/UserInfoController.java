package org.doraemon.framework.ssm.web.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.dubbo.config.annotation.DubboReference;
import org.doraemon.framework.ssm.service.DemoService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @description: 描述
 * @author: fengwenping
 * @date: 2021-11-06 17:41
 */
@RestController
@RequestMapping("/userInfo")
@Api(tags = {"用户/会员信息服务"})
public class UserInfoController {

    private final DemoService demoService;

    public UserInfoController(@Qualifier("demoService") DemoService demoService) {
        this.demoService = demoService;
    }

    @GetMapping("/queryMemberInfo")
    @ApiOperation(value = "查询会员信息", notes = "查询会员信息注意点")
    public String queryMemberInfo() {
        return this.demoService.sayHello("zhangsan");
    }

    @PostMapping("/queryUserInfoList")
    @ApiOperation(value = "查询用户信息列表", notes = "查询用户信息列表注意点")
    public List<String> queryUserInfoList() {
        return Collections.singletonList(this.demoService.sayHello("list"));
    }

    @DeleteMapping("/deleteUserInfo")
    @ApiOperation(value = "根据Id删除用户信息", notes = "用户的Id不能为空")
    public String deleteUserInfo(@RequestParam(value = "id") String id) {
        return this.demoService.sayHello(id);
    }

    @PutMapping("/modifyUserInfo")
    @ApiOperation(value = "根据Id修改用户信息", notes = "用户的Id不能为空")
    public String modifyUserInfo(@RequestParam(value = "id") String id) {
        return this.demoService.sayHello(id);
    }

}
